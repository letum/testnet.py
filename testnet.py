#!/usr/bin/env python3
import argparse
import base64
import getpass
import hashlib
import json
import logging
import os
import shutil
import signal
import struct
import subprocess
import sys
import time

if sys.version_info.major < 3:
    print("Python 2 is not supported, use python3")
    sys.exit(1)

from contextlib import suppress


def fatal_error(msg):
    """Show an error and exit"""
    logging.error(msg)
    sys.exit(1)


def load_config(path):
    if not os.path.exists(path) or not os.access(path, os.R_OK):
        fatal_error("Can't read {}".format(path))

    try:
        with open(path, "r") as f:
            c = f.read()
        config = json.loads(c)
    except:
        fatal_error("Error parsing config file")

    for v in ["i2p_install_path", "i2pd_binary"]:
        if v in config and not os.path.exists(config[v]):
            fatal_error("'{}' doesn't exist".format(config[v]))

    return config


def run(command):
    """Execute a command in the shell, return the exit code"""
    logging.debug("Try running: " + command)
    res = subprocess.run(
        command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    if res.returncode != 0:
        logging.error(
            "Running >" + command + "< created the following error" + str(res.stderr)
        )
    return res.returncode


def popen(command):
    """Execute a command in the shell, return the exit code"""
    p = subprocess.Popen(command)
    return p.returncode


def ns_popen(namespace, command):
    """Execute a command in the network namespace as root"""
    cmd = ["sudo", "-n", "ip", "netns", "exec", namespace, command]
    return popen(cmd)


def ns_popen_as_user(namespace, command):
    """Execute a command in the network namespace as a current user"""
    cmd = [
        "sudo",
        "-n",
        "ip",
        "netns",
        "exec",
        namespace,
        "su",
        "-c",
        command,
        getpass.getuser(),
    ]
    return popen(cmd)


def ns_run(namespace, command):
    """Execute a command in the network namespace as root"""
    return run("sudo -n ip netns exec {} {}".format(namespace, command))


def ns_run_as_user(namespace, command):
    """Execute a command in the network namespace as a current user"""
    return run(
        "sudo -n ip netns exec {} su -c '{}' {}".format(
            namespace, command, getpass.getuser()
        )
    )


def ri_b64hash(path):
    """Get Base64 encoded hash of a router from a RouterInfo file"""
    with open(path, "rb") as f:
        data = f.read()
    cert_len = struct.unpack("!H", data[385:387])[0]
    public_data = data[: 387 + cert_len]
    test = hashlib.sha256(public_data).digest()
    return base64.b64encode(test, altchars=b"-~").decode()


def is_ready(path):
    """Check if router.info file is ready for use in a reseed"""
    ready = False
    if os.path.exists(path):
        with open(path, "rb") as f:
            data = f.read()
        ssu_ready = b"\x63\x61\x70\x73\x3d\x02\x42\x43\x3b"
        ntcp_ready = b"\x63\x61\x70\x73\x3d\x03\x50\x66\x52\x3b"
        ready = ssu_ready in data or ntcp_ready in data
    return ready


def create_gateway_namespace(namespace):
    """Setup gateway network namespace"""
    run("sudo -n ip netns add {}".format(namespace))
    ns_run(namespace, "ip link set dev lo up")
    ns_run(namespace, "sysctl net.ipv4.conf.all.forwarding=1")
    ns_run(namespace, "sudo sysctl net.ipv4.ping_group_range='0 2147483647'")


def add_network_bridge_to_gateway(
    gateway_ns, network, network_gateway_address, network_gateway_mask
):
    """Add bridge interface for the network"""
    network_gateway = network_gateway_address + network_gateway_mask
    ns_run(gateway_ns, "ip link add name br{} type bridge".format(network))
    ns_run(gateway_ns, "ip link set br{} up".format(network))
    ns_run(gateway_ns, "ip address add {} dev br{}".format(network_gateway, network))


def create_router_namespace(name, ip, network_name, network, gateway):
    """Setup I2P router network namespace"""
    logging.info("Create namespace for router " + name)
    network_subnet = network["subnet"] + network["subnet_mask"]
    run("sudo -n ip netns add {}".format(name))
    run(
        "sudo -n ip link add eth0 netns {} type veth peer netns {} name veth-{}".format(
            name, gateway, name
        )
    )
    ns_run(name, "ip link set dev lo up")
    ns_run(name, "ip link set dev eth0 up".format(name))
    ns_run(name, "ip address add {} dev eth0".format(ip))
    ns_run(gateway, "ip link set veth-{} up".format(name))
    ns_run(gateway, "ip link set veth-{} master br{}".format(name, network_name))
    ns_run(
        name,
        "ip route add {} dev eth0 proto kernel scope link src {}".format(
            network_subnet, ip
        ),
    )
    ns_run(name, "ip route add default via {} dev eth0".format(network["gateway"]))
    ns_run(name, "sudo sysctl net.ipv4.ping_group_range='0 2147483647'")


def start_i2p(namespace, i2p_install_path, datadir):
    """Prepare datadir and start I2P router in its own network namespace"""
    for t in [
        "certificates",
        "docs",
        "eepget",
        "eepsite",
        "geoip",
        "history.txt",
        "hosts.txt",
        "lib",
        "locale",
        "webapps",
    ]:
        os.symlink(os.path.join(i2p_install_path, t), os.path.join(datadir, t))

    for t in ["i2psvc", "libwrapper.so"]:
        os.symlink(
            os.path.join(i2p_install_path, "lib/wrapper/linux64", t),
            os.path.join(datadir, t),
        )

    with open(os.path.join(datadir, "wrapper.config"), "w") as wt:
        with open(os.path.join(i2p_install_path, "wrapper.config"), "r") as ws:
            for l in ws.readlines():
                if not l.startswith("#"):
                    wt.write(l.replace("$INSTALL_PATH", datadir))

            extra_config = {
                "3": "i2p.dir.pid=logs",
                "4": "i2p.dir.temp=tmp",
                "5": "i2p.dir.config={}".format(datadir),
                "6": "router.pingFile={}/router.ping".format(datadir),
            }

            for k, v in extra_config.items():
                wt.write("wrapper.java.additional.{}=-D{}\n".format(k, v))
                wt.write("wrapper.java.additional.{}.stripquotes=TRUE\n".format(k))

    with open(os.path.join(datadir, "i2prouter"), "w") as wt:
        with open(os.path.join(i2p_install_path, "i2prouter"), "r") as ws:
            for l in ws.readlines():
                if not l.startswith("#"):
                    if l.startswith("I2PTEMP="):
                        wt.write("I2P={}\nI2P_CONFIG_DIR={}\n".format(datadir, datadir))
                    else:
                        wt.write(l)

    command = "/bin/sh {} start".format(os.path.join(datadir, "i2prouter"))
    ns_run_as_user(namespace, command)


def start_reseed_server(namespace, conf):
    """Start reseed server"""
    stop_reseed_server()
    reseed_server_binary = conf["reseed_server_binary"]
    reseed_server_args = conf["reseed_server_args"]
    command = "{} {}".format(reseed_server_binary, " ".join(reseed_server_args))
    ns_popen_as_user(namespace, command)


def stop_reseed_server():
    """Stop reseed server"""
    # TODO: maybe there is a better way?
    run("killall i2p-tools")


def stop_i2p(datadir):
    command = "/bin/sh {} stop".format(os.path.join(datadir, "i2prouter"))
    run(command)


def start_i2pd(namespace, binary, args, datadir):
    command = "{} {}".format(binary, " ".join(args))
    ns_run_as_user(namespace, command)


def stop_i2pd(datadir):
    pidfile = os.path.join(datadir, "i2pd.pid")
    if os.path.exists(pidfile):
        with open(pidfile) as f:
            with suppress(ProcessLookupError):
                pid = int(f.read().strip())
                os.kill(pid, signal.SIGKILL)


def start_routers(routers, conf):
    """Stars all routers from a list"""
    for router in routers:
        start_router(conf, router)


def stop_routers(routers, conf):
    """Stops all routers from a list"""
    for router in routers:
        stop_router(conf, router)
        if os.path.exists(os.path.join(conf["workspace"], router["name"])):
            shutil.rmtree(os.path.join(conf["workspace"], router["name"]))


def restart_routers(routers, conf):
    for router in routers:
        stop_router(conf, router)
        start_router(conf, router)


def _dec_to_binary(ip_address):
    return list(map(lambda x: bin(x)[2:].zfill(8), ip_address))


def _negation_mask(net_mask):
    wild = list()
    for i in net_mask:
        wild.append(255 - int(i))
    return wild


def get_net_mask(cidr):
    mask = [0, 0, 0, 0]
    for i in range(int(cidr)):
        mask[int(i / 8)] += 1 << (7 - i % 8)
    return mask


def get_broadcast_ip(binary_ip, negation_mask):
    broadcast = list()
    for x, y in zip(binary_ip, negation_mask):
        broadcast.append(int(x, 2) | int(y, 2))
    return broadcast


def get_network_id(binary_ip, binary_mask):
    network = list()
    for x, y in zip(binary_ip, binary_mask):
        network.append(int(x, 2) & int(y, 2))
    return network


def get_number_of_host(negation_mask):
    return (2 ** sum(map(lambda x: sum(c == "1" for c in x), negation_mask))) - 2


def generate_routers(router_from_types, config):
    """Generates a list of routers based on the given config"""
    logging.info("Generate router list")
    routers = []
    octects = [16777215, 65535, 255, 0]

    for router_from_type in router_from_types:
        quantity = router_from_type["quantity"] if "quantity" in router_from_type else 1
        router_network = config["networks"][router_from_type["network"]]
        router_network_id = router_network["subnet"]

        if "node_offset" in router_network:
            node_offset = router_network["node_offset"]
        elif "node_offset" in config:
            node_offset = config["node_offset"]
        else:
            node_offset = 1

        i2pd_custom_args = (
            router_from_type["i2pd_custom_args"]
            if "i2pd_custom_args" in router_from_type
            else []
        )

        for router_index in range(quantity):
            router_name = router_from_type["network"] + str(router_index)
            if "floodfill" in router_from_type and router_from_type["floodfill"]:
                router_name = "-".join([router_name, "f"])
            if "reseed" in router_from_type and router_from_type["reseed"]:
                router_name = "-".join([router_name, "r"])

            router_ip = list(map(int, router_network_id.split(".")))

            for index, octect in enumerate(octects):
                router_ip_octect = router_index + node_offset
                if router_ip_octect >= octect:
                    router_ip[index] = router_ip[index] + router_ip_octect - octect
                    router_network["node_offset"] = router_network["node_offset"] + 1
                    break
            node = {
                "router": router_from_type["router"],
                "name": router_name,
                "ip": ".".join(map(str, router_ip)),
                "i2pd_custom_args": i2pd_custom_args,
                "network": router_from_type["network"],
                "i2pd_binary": router_from_type["i2pd_binary"]
                if "i2pd_binary" in router_from_type
                else config["i2pd_binary"],
                "floodfill": router_from_type["floodfill"]
                if "floodfill" in router_from_type
                else False,
                "reseed": router_from_type["reseed"]
                if "reseed" in router_from_type
                else False,
            }
            routers.append(node)
    return routers


def start_router(conf, node):
    """Prepare environment for a router based on it's configuration and start it"""
    create_router_namespace(
        node["name"],
        node["ip"],
        node["network"],
        conf["networks"][node["network"]],
        conf["gateway"],
    )

    datadir = os.path.abspath(os.path.join(conf["workspace"], node["name"]))
    if not os.path.exists(datadir):
        os.mkdir(datadir)

    if node["router"] == "i2pd":
        binary = node["i2pd_binary"] if "i2pd_binary" in node else conf["i2pd_binary"]
        args = []

        if "i2pd_binary" in conf.keys():
            reseed_server = (
                "https://" + conf["networks"][node["network"]]["gateway"] + ":8443/"
            )
            args += [
                "--reseed.urls",
                reseed_server,  # Reseed URLs, separated by comma
            ]
            args += conf["i2pd_reseed_args"]

        args += node["i2pd_args"] if "i2pd_args" in node else conf["i2pd_args"]
        args += [
            "--datadir",
            datadir,  # Path to storage of i2pd data (RI, keys, peer profiles, ...)
            "--host",
            node["ip"],
            "--daemon",  # Router will go to background after start
        ]
        if "i2pd_custom_args" in node:
            args += node["i2pd_custom_args"]
        if "floodfill" in node and node["floodfill"] == True:
            args += ["--floodfill"]
        if conf["debug_logging"]:
            args += ["--loglevel", "debug"]

        start_i2pd(node["name"], binary, args, datadir)
    else:
        ip = node["ip"].split("/")[0]
        options = node["i2p_options"] if "i2p_options" in node else conf["i2p_options"]
        if "custom_options" in node:
            options += node["custom_options"]
        options.extend(
            [
                "i2np.allowLocal=true",
                "i2np.udp.bindInterface={}".format(ip),
                "i2np.ntcp.bindInterface={}".format(ip),
            ]
        )
        if "floodfill" in node and node["floodfill"] == True:
            options.append("router.floodfillParticipant=true")

        with open(os.path.join(datadir, "router.config"), "w") as f:
            for o in options:
                f.write("{}\n".format(o))

        if conf["debug_logging"]:
            with open(os.path.join(datadir, "logger.config"), "w") as f:
                f.write("logger.defaultLevel=DEBUG")

        with open(os.path.join(datadir, "clients.config"), "w") as f:
            clients = (
                node["clients_config"]
                if "clients_config" in node
                else conf["clients_config"]
            )
            for c in clients:
                f.write("{}\n".format(c))

        start_i2p(node["name"], conf["i2p_install_path"], datadir)


def stop_router(config, node):
    datadir = os.path.abspath(os.path.join(config["workspace"], node["name"]))
    if node["router"] == "i2pd":
        stop_i2pd(datadir)
    else:
        stop_i2p(datadir)

    run("sudo -n ip netns del {}".format(node["name"]))


def count_routerinfos(netdb):
    """Count router info files in the netDb directory"""
    i = 0
    if os.path.exists(netdb):
        for d in os.walk(netdb):
            i += len(d[2])
    return i


def router_status(config, node):
    state = "down"
    info = "not running"

    datadir = os.path.abspath(os.path.join(config["workspace"], node["name"]))
    if os.path.exists(datadir):
        if node["router"] == "i2pd":
            pidfile = os.path.join(datadir, "i2pd.pid")
            logfile = os.path.join(datadir, "i2pd.log")
        else:
            pidfile = os.path.join(datadir, "i2p.pid")
            logfile = os.path.join(datadir, "logs", "log-router-0.txt")

        if os.path.exists(pidfile):
            with open(pidfile, "r") as f:
                pid = f.read().strip()
            if os.path.exists("/proc/{}".format(pid)):
                state = "up"
            info = "Known Routers: {}\nIP: {} Network: {}\nPID: {}".format(
                count_routerinfos(os.path.join(datadir, "netDb")),
                node["ip"],
                node["network"],
                pid,
            )

        info += "\nDatadir: {} Log: {}".format(datadir, logfile)
    return """{} is {}\n{}\n""".format(node["name"], state, info)


def make_netdb_dir(conf, routers):
    """Create reseed netDb dir and populate it with router.info's from reseed nodes"""
    logging.info("Creating netdb for reseeding")
    netdb_dir = os.path.abspath(os.path.join(conf["workspace"], "netDb"))
    if os.path.exists(netdb_dir):
        shutil.rmtree(netdb_dir)

    os.mkdir(netdb_dir)

    for n in routers:
        ri_path = os.path.join(conf["workspace"], n["name"], "router.info")
        for x in range(1200):  # two minutes max
            if is_ready(ri_path):
                ri_hash = ri_b64hash(ri_path)
                t_dir = os.path.join(netdb_dir, "r{}".format(ri_hash[0]))
                if not os.path.exists(t_dir):
                    os.mkdir(t_dir)
                shutil.copy(
                    ri_path, os.path.join(t_dir, "routerInfo-{}.dat".format(ri_hash))
                )
                break
            else:
                time.sleep(0.1)
    logging.info("Finished creating netdb for reseeding")


def update_network_informations(conf):
    networks = conf["networks"]
    for network in networks:
        network_conf = conf["networks"][network]
        address = map(int, network_conf["gateway"].split("."))
        cidr = network_conf["subnet_mask"].split("/")[1]
        network_mask = get_net_mask(cidr)

        binary_mask = _dec_to_binary(network_mask)
        negation_mask = _dec_to_binary(_negation_mask(network_mask))
        binary_ip = _dec_to_binary(address)

        network_id = get_network_id(binary_ip, binary_mask)
        broadcast_ip = get_broadcast_ip(binary_ip, negation_mask)
        number_of_host = get_number_of_host(negation_mask)

        network_conf["network_mask"] = network_mask
        network_conf["subnet"] = ".".join(map(str, network_id))
        network_conf["number_of_host"] = number_of_host
        network_conf["broadcast_ip"] = ".".join(map(str, broadcast_ip))
        logging.info("Updated network information")


### tool actions


def start(args):
    conf = load_config(args.config)
    update_network_informations(conf)

    if not os.path.exists(conf["workspace"]):
        os.mkdir(conf["workspace"])
    create_gateway_namespace(conf["gateway"])

    for network in conf["networks"].keys():
        add_network_bridge_to_gateway(
            conf["gateway"],
            network,
            conf["networks"][network]["gateway"],
            conf["networks"][network]["subnet_mask"],
        )

    routers = generate_routers(conf["router_types"], conf)
    reseed_routers = list(filter(lambda router: router["reseed"], routers))

    start_routers(reseed_routers, conf)
    make_netdb_dir(conf, reseed_routers)
    restart_routers(reseed_routers, conf)
    # TODO: feature reseeding without a reseed server is missing
    if "i2pd_binary" in conf.keys():
        start_reseed_server(conf["gateway"], conf)

    start_routers(routers, conf)

    logging.info("Your testnet should be up and running")


def stop(args):
    conf = load_config(args.config)
    update_network_informations(conf)
    routers = generate_routers(conf["router_types"], conf)
    stop_routers(routers, conf)

    run("sudo -n ip netns del {}".format(conf["gateway"]))
    stop_reseed_server()
    with suppress(FileNotFoundError):
        shutil.rmtree(os.path.join(conf["workspace"], "netDb"))


def status(args):
    conf = load_config(args.config)
    update_network_informations(conf)
    print("Network: ")
    for net in conf["networks"].keys():
        num = len([n for n in conf["nodes"] if n["network"] == net])
        print("{}: {} has {} nodes".format(net, conf["networks"][net]["subnet"], num))
    print("\nRouters:")
    for n in conf["nodes"]:
        print(router_status(conf, n))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", "-d", action="store_true", help="Debug output")

    subparsers = parser.add_subparsers(title="actions", help="Command to execute")

    start_parser = subparsers.add_parser(
        "start", description="starts a testnet", usage="%(prog)s config.json"
    )
    start_parser.add_argument("config", metavar="CONFIG", help="Config file")
    start_parser.set_defaults(func=start)

    stop_parser = subparsers.add_parser(
        "stop", description="stops a testnet", usage="%(prog)s config.json"
    )
    stop_parser.add_argument("config", metavar="CONFIG", help="Config file")
    stop_parser.set_defaults(func=stop)

    status_parser = subparsers.add_parser(
        "status", description="print testnet status", usage="%(prog)s config.json"
    )
    status_parser.add_argument("config", metavar="CONFIG", help="Config file")
    status_parser.set_defaults(func=status)

    args = parser.parse_args()
    logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO)

    if not sys.platform.startswith("linux"):
        fatal_error("This script only runs on Linux, your OS is not supported")

    if os.getuid() == 0:
        fatal_error("You MUST run this script as a non-root user")

    if not os.path.exists("/usr/bin/sudo"):
        fatal_error("/usr/bin/sudo is missing, please install 'sudo' package")

    res = subprocess.run(
        "sudo -n -l", shell=True, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL
    )
    if res.returncode != 0:
        fatal_error("sudo is not configured correctly, read the manual")

    if hasattr(args, "func"):
        args.func(args)
    else:
        parser.print_help()
